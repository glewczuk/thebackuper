package com.backuper.Controllers;

import java.util.List;

import com.backuper.Model.CurrentUser;
import com.backuper.Model.MyFile;
import com.backuper.Model.DTOs.MyFileDTO;
import com.backuper.Model.Services.FileService;
import com.backuper.Model.Services.UserService;
import com.backuper.Model.User;
import org.springframework.beans.factory.annotation.Autowired;import org.springframework.stereotype.Controller;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;

@Controller
public class SimpleController {

    String UPLOADED_FOLDER = "//";

    @Autowired
    UserService userService;

    @Autowired
    FileService fileService;

    @RequestMapping(value={"/", "/index"})
    public String greeting(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "html/index";
    }

    @RequestMapping("/login")
    public String login(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            return "forward:/index";
        }
        model.addAttribute("name", name);
        return "html/login";
    }

    @RequestMapping("/register")
    public String register(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "html/register";
    }

    @RequestMapping(method=RequestMethod.POST, value="/register", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String registerAdd(@ModelAttribute User user, Model model) {
        User realUser = new User(user.getLogin(), user.getPassword());
        if(userService.getUserByUsername(realUser.getLogin()) == null) {
            userService.addUser(realUser);
            model.addAttribute("success", true);
            model.addAttribute("failed", false);
        }
        else {
            model.addAttribute("failed", true);
            model.addAttribute("success", false);
        }
        return "html/register";
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";//You can redirect wherever you want, but generally it's a good practice to show login screen again.
    }

    @RequestMapping("/files")
    public String userFiles(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
        Object auth = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        CurrentUser cU = (CurrentUser)auth;
        Integer userId = cU.getId().intValue();
        System.out.println(userId);
        List<MyFileDTO> list = fileService.getAllFilesOfUser(userId);
        for (MyFileDTO i : list) {
            System.out.println(i.getFile());
        }
        model.addAttribute("userId", userId);
        model.addAttribute("files", list);
        return "html/files";
    }

    @PostMapping("/uploadfile")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) throws IOException, NoSuchAlgorithmException {

        byte[] bytes = file.getBytes();
        Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
        Files.write(path, bytes);

        Object auth = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        CurrentUser cU = (CurrentUser)auth;
        User u = cU.getUser();
        MyFile mf = new MyFile(UPLOADED_FOLDER+file.getOriginalFilename(), u);
        fileService.addFile(mf);
        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded '" + file.getOriginalFilename() + "'");
        return "redirect:/files";
    }

    @RequestMapping("/file")
    public String uploadFiles() {
        return "html/uploadfile";
    }

    @RequestMapping(value = "/uploadfiles", method = RequestMethod.POST)
    public String uploadFiles(@RequestParam("uploadingFiles") MultipartFile[] uploadingFiles,
                              RedirectAttributes redirectAttributes) throws IOException, NoSuchAlgorithmException {

        for(MultipartFile file : uploadingFiles) {
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
            Files.write(path, bytes);

            Object auth = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            CurrentUser cU = (CurrentUser)auth;
            User u = cU.getUser();
            MyFile mf = new MyFile(UPLOADED_FOLDER+file.getOriginalFilename(), u);
            System.out.println("xD" + mf.getName());
            fileService.addFile(mf);
        }
        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded files");
        return "redirect:/files";
    }

    @RequestMapping(method= RequestMethod.POST, value="/user/{userId}/file/{documentId}/delete")
    public String deleteFile(@PathVariable Integer userId, @PathVariable Integer documentId) throws IOException, NoSuchAlgorithmException {
        MyFile file = fileService.getUserFile(userId, documentId);
        Object auth = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        CurrentUser cU = (CurrentUser)auth;
        Integer thisUserId = cU.getId().intValue();
        if(file.getUser().getId().equals(thisUserId))
            fileService.deleteFile(file);
        return "redirect:/files";
    }
}
