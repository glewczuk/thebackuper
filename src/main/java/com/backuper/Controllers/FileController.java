package com.backuper.Controllers;

import com.backuper.Model.CurrentUser;
import com.backuper.Model.MyFile;
import com.backuper.Model.DTOs.MyFileDTO;
import com.backuper.Model.Services.FileService;
import com.backuper.Model.Services.UserService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@RestController
public class FileController {

    @Autowired
    private FileService fileService;

    @Autowired
    private UserService userService;

    @RequestMapping("/user/{id}/files")
    public List<MyFileDTO> getAllFilesOfUser(@PathVariable Integer id) {
        return fileService.getAllFilesOfUser(id);
    }

    @RequestMapping("/api/user/{id}/files")
    public List<MyFileDTO> getAllFilesOfUserApi(@PathVariable Integer id) {
        return fileService.getAllFilesOfUser(id);
    }

    @RequestMapping("user/{userId}/file/{documentId}")
    public void download(@PathVariable Integer userId, @PathVariable Integer documentId, HttpServletResponse response) throws IOException, NoSuchAlgorithmException {

        MyFile file = fileService.getUserFile(userId, documentId);
        try {
            response.setHeader("Content-Disposition", "attachment;filename=\"" + file.getFile().getName()+ "\"");
            OutputStream out = response.getOutputStream();
            response.setContentType("file");
            ByteArrayInputStream baos = new ByteArrayInputStream(file.getFileContent());
            IOUtils.copy(baos , out);
            out.flush();
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(method= RequestMethod.POST, value="/file")
    public void addFile(@RequestBody MyFile file) throws IOException, NoSuchAlgorithmException {
        fileService.addFile(file);
    }
}
