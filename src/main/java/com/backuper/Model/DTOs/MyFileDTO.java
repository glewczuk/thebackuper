package com.backuper.Model.DTOs;

import com.backuper.Model.MyFile;

import java.io.File;

public class MyFileDTO {
    private Integer id;
    private Integer userId;
    private File file;
    private String checkSum;
    private String type;
    private String name;
    private String mac;
    // Path where root is specified directory
    private String originalPath;

    private String lastModified;

    public MyFileDTO(MyFile file) {
        id = file.getId();
        userId = file.getUser().getId();
        this.file = file.getFile();
        checkSum = file.getCheckSum();
        type = file.getType();
        name = file.getName();
        mac = file.getMac();
        originalPath = file.getOriginalPath();
        lastModified = file.getLastModified();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(String checkSum) {
        this.checkSum = checkSum;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getOriginalPath() {
        return originalPath;
    }

    public void setOriginalPath(String originalPath) {
        this.originalPath = originalPath;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

}

