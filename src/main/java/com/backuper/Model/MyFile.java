package com.backuper.Model;

import javax.persistence.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Entity
public class MyFile {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    @ManyToOne
    private User user;
    private File file;
    @Lob @Basic(fetch = FetchType.LAZY)
    @Column(length=5677700)
    private byte[] fileContent;
    private String checkSum;
    private String type;
    private String name;
    private String mac;
    private String originalPath;
    private String lastModified;

    public MyFile() {}

    public MyFile(String path, User user) throws IOException, NoSuchAlgorithmException {
        file = new File(path);
        this.user = user;
        FileInputStream fileInputStream = new FileInputStream(file);
        int byteLength = (int)file.length();
        fileContent = new byte[byteLength];
        fileInputStream.read(fileContent,0,byteLength);
        this.checkSum = countCheckSum();
        this.type = extractType();
        this.name = extractName();
        this.mac = "fakeMac";
        this.originalPath = "fakePath";
        this.lastModified = "0";
    }

    public MyFile(String path, User user, String mac, String originalPath, String lastModified) throws IOException, NoSuchAlgorithmException {
        this(path, user);
        this.mac = mac;
        this.originalPath = originalPath;
        this.lastModified = lastModified;
    }

    public String countCheckSum() throws NoSuchAlgorithmException {
        byte[] a = MessageDigest.getInstance("MD5").digest(fileContent);
        return toHexString(a);
    }

    private static String toHexString(byte[] bytes) {
        StringBuilder hexString = new StringBuilder();

        for(int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public byte[] getFileContent() {
        return fileContent;
    }

    public void setFileContent(byte[] fileContent) {
        this.fileContent = fileContent;
    }

    public String getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(String checkSum) {
        this.checkSum = checkSum;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getOriginalPath() {
        return originalPath;
    }

    public void setOriginalPath(String originalPath) {
        this.originalPath = originalPath;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }


    private boolean isMusic(String type) {
        Set<String> set = new HashSet<String>(Arrays.asList(
                "mp3", "wma", "wv", "tta",
                "wav", "3gp", "gsm", "m4a"
        ));
        return (set.contains(type));
    }

    private boolean isText(String type) {
        Set<String> set = new HashSet<String>(Arrays.asList(
                "txt", "pdf", "zip", "tar", "xml",
                "doc", "pptx", "ppt", "xlsx"
        ));
        return (set.contains(type));
    }

    private boolean isImage(String type) {
        Set<String> set = new HashSet<String>(Arrays.asList(
                "jpg", "jpeg", "gif", "bmp", "png", "bat"
        ));
        return (set.contains(type));
    }

    private boolean isVideo(String type) {
        Set<String> set = new HashSet<String>(Arrays.asList(
                "flv", "avi", "wmv", "amv", "mp4",
                "m4v", "mpg"
        ));
        return (set.contains(type));
    }

    private String extractType() {
        String name = this.extractName();
        String [] list = name.split("\\.");
        String type;
        System.out.println(name);
        System.out.println(list.length);
        if(list.length > 0) {
            type = list[(list.length-1)];
            System.out.println(type);
        }
        else
            return "file";
        if(isImage(type)) return "img";
        else if(isVideo(type)) return "video";
        else if(isText(type)) return "text";
        else if(isMusic(type)) return "music";
        else return "file";
    }

    private String extractName() {
        String name = this.file.getName();
        String [] list = name.split("/");
        return list[(list.length-1)];
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyFile myFile = (MyFile) o;

        if (id != null ? !id.equals(myFile.id) : myFile.id != null) return false;
        if (user != null ? !user.equals(myFile.user) : myFile.user != null) return false;
        if (file != null ? !file.equals(myFile.file) : myFile.file != null) return false;
        if (!Arrays.equals(fileContent, myFile.fileContent)) return false;
        return checkSum != null ? checkSum.equals(myFile.checkSum) : myFile.checkSum == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (file != null ? file.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(fileContent);
        result = 31 * result + (checkSum != null ? checkSum.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MyFile{" +
                "id=" + id +
                ", user=" + user +
                ", file=" + file +
                ", fileContent=" + Arrays.toString(fileContent) +
                ", checkSum='" + checkSum + '\'' +
                '}';
    }
}