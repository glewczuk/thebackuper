package com.thebackuper

import java.io.File
import java.net.NetworkInterface
import java.net._
import java.nio.ByteBuffer
import java.net.NetworkInterface
import java.util
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import java.util.UUID
import sys.process._
import java.net.URL
import java.io.File
import java.security.{MessageDigest, NoSuchAlgorithmException}

class FileService {

  def recursiveListFiles(directory: File): Array[File] = {
    val these = directory.listFiles
    val partialResult = these.filter(!_.isDirectory)
    partialResult ++ these.filter(_.isDirectory).flatMap(recursiveListFiles)
  }

  def compareFilesAge(localFile: File, remoteFile: File) = {
    localFile.lastModified >= remoteFile.lastModified
  }

  def createMAC: String = {
      NetworkInterface.getNetworkInterfaces.toList
        .filter(_.getHardwareAddress != null).head
          .getHardwareAddress.map(el =>
            String.format("%02x", el.asInstanceOf[AnyRef]))
             .mkString("")
  }


  private def countCheckSum(fileContent: Array[Byte]) = {
    val a = MessageDigest.getInstance("MD5").digest(fileContent)
    toHexString(a)
  }

  private def toHexString(bytes: Array[Byte]) = {
    val hexString = new StringBuilder
    var i = 0
    while ( {
      i < bytes.length
    }) {
      val hex = Integer.toHexString(0xFF & bytes(i))
      if (hex.length == 1) hexString.append('0')
      hexString.append(hex) {
        i += 1; i - 1
      }
    }
    hexString.toString
  }

  def downloadFile(url: String, filename: String) = {
    new URL(url) #> new File(filename) !!
  }

  def deleteLocalFile(path: String) = {
    new File(path).delete
  }

  def getRelativePath(pathOfDirectory: String, pathOfFile: String): String = {
    val pathDList = pathOfDirectory.split("/")
    val pathFList = pathOfFile.split("/")
    pathFList.zipAll(pathDList, "", "").toList.filter((pair) => pair._1 != pair._2).map(_._1).mkString("/")
  }
}
