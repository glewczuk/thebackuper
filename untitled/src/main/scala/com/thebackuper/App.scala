package com.thebackuper

import sys.process._
import java.net.URL
import java.io.{File, PrintWriter}
import java.nio.file.Files

import akka.actor.ActorSystem

import scala.io._
import akka.http.scaladsl.settings.ParserSettings.ErrorLoggingVerbosity.Full
import com.softwaremill.macwire._
import scala.reflect.io.Streamable.Bytes
import akka.actor.Actor
import akka.actor.Props
import scala.concurrent.duration._


object App {

  val fileService = wire[FileService]
  val connector = wire[Connector]
  val baseAddress = "http://localhost:8080"
  val prefixApi = "/api/user/"
  val sep = java.io.File.separator

  def proccessData(args0: String, args1: String) = {
    println("JESTEM!")
    val fullDirectory = args0 // //home//jan//Documents//storage
    val userName = args1
    val directory = fullDirectory.split("/").toList.last //storage
    val lastSynchronize = new File(fullDirectory + "/.lastSynchronize")
    lastSynchronize.createNewFile()
    println(fullDirectory)
    println(lastSynchronize.exists())
    val source = Source.fromFile(fullDirectory + "/.lastSynchronize")
    val lastSynchronizeFiles = try source.mkString.split("\n") finally source.close()

    val localFilesWithSynchronizeFile = fileService.recursiveListFiles(new File(fullDirectory))
    val localFiles = localFilesWithSynchronizeFile.filter(_.getName != ".lastSynchronize")
    val mac = directory //fileService.createMAC
    val remoteFiles = connector.getFiles(baseAddress + "/api/user/" + userName + "/file")
    val localFilesNames = localFiles.map(_.getName).toSet
    val remoteFilesNames = remoteFiles.map(_.name).toSet
    val sameFilesSet = localFilesNames.intersect(remoteFilesNames)
    val onlyRemotes = remoteFilesNames.diff(localFilesNames)
    val onlyLocal = localFilesNames.diff(remoteFilesNames)

    remoteFiles foreach (x =>
      if (x.originalPath == "fakePath" || x.mac == "fakeMac" || x.lastModified == "0") {
        fileService.downloadFile(baseAddress + prefixApi + userName + "/file/" + x.id, fullDirectory + "/" + x.name)
        connector.deleteFile(userName, x.id)
        connector.uploadFile(baseAddress + prefixApi + userName + "/uploadfile/" + mac + "/" +
          x.name + "/" + new File(fullDirectory + "/" + x.name).lastModified().toString+"/", x, fullDirectory)
        println(x.name)
      }
      else {
        val remoteToDeleteOrDownload = onlyRemotes.flatMap(name => remoteFiles.find(_.name == name))
          .partition(_.mac == mac)
        remoteToDeleteOrDownload._1.map {
          x =>  {
            println("1")
            connector.deleteFile(userName, x.id)
          }
        }
        remoteToDeleteOrDownload._2.map {
          x => {
            println("2")
            fileService.downloadFile(baseAddress + prefixApi + userName + "/file/" + x.id, fullDirectory + "//" + x.name)
          }
        }
      })

    val localToDeleteOrUpload = onlyLocal.flatMap(name => {
      localFiles.find(_.getName == name)
    })
      .partition(file =>
        lastSynchronizeFiles.contains {
          fileService.getRelativePath(fullDirectory, file.getAbsolutePath)
        })
    localToDeleteOrUpload._1 foreach (x => {
      fileService.deleteLocalFile(x.getAbsolutePath)
    })
    localToDeleteOrUpload._2 foreach (x => {
      connector.uploadNewFile(x, fileService.getRelativePath(fullDirectory, x.getAbsolutePath),
        userName, mac, fullDirectory)
    })

    val sameFilesRemote = sameFilesSet.map(name => remoteFiles.find(_.name == name))
    val sameFilesLocal = sameFilesSet.map(name => localFiles.find(_.getName == name))
    sameFilesSet.foreach(name => {
      val rem = remoteFiles.find(_.name == name).get
      val loc = localFiles.find(_.getName == name).get
      println(rem.lastModified + " " + loc.lastModified().toString)
      println(loc.lastModified().toString.compareTo(rem.lastModified))
      if (loc.lastModified().toString.compareTo(rem.lastModified) < 0) {
        fileService.downloadFile(baseAddress + prefixApi + userName + "/file/" + rem.id, fullDirectory + "/" + rem.name)
        println(loc.lastModified().toString + rem.lastModified)
      }
      else if(loc.lastModified().toString.compareTo(rem.lastModified) > 0) {
        println(loc.lastModified().toString + rem.lastModified)
        connector.deleteFile(userName, rem.id)
        connector.uploadFile(baseAddress + prefixApi + userName + "/uploadfile/" + mac + "/" +
          rem.name + "/" + loc.lastModified.toString+"/", rem, fullDirectory)
      }
    })

    val pw = new PrintWriter(lastSynchronize.getAbsolutePath)
    pw.write(fileService.recursiveListFiles(new File(fullDirectory)).filter(_.getName != ".lastSynchronize")
      .map(x => fileService.getRelativePath(fullDirectory, x.getAbsolutePath)).mkString("\n"))
    pw.close()
    println("Done!")
  }

  def main(args: Array[String]) {
    if(args.length < 2) {
      print("Wrong number of arguments. Specify your directory and user name.")
    }
    else {
      val system = ActorSystem("mySystem")
      import system.dispatcher
      system.scheduler.schedule(1 seconds, 5 seconds) {
        println("start")
        proccessData(args(0), args(1))
        println("elko")
      }
    }
  }
}
