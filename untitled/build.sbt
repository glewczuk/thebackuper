name := "basic-project"

organization := "example"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.11.2"

crossScalaVersions := Seq("2.10.4", "2.11.2")

libraryDependencies ++= Seq(
  "org.scalatest"             %% "scalatest"              % "2.2.1"   % "test",
  "org.scalacheck"            %% "scalacheck"             % "1.11.5"  % "test",
  "com.softwaremill.macwire"  % "macros_2.11"             % "2.3.0",
  "org.scalaj"                % "scalaj-http_2.11"        % "2.3.0",
  "com.typesafe.akka"         %% "akka-http-spray-json"   % "10.0.6",
  "com.typesafe.play"         %% "play-json"              % "2.5.1",
  "com.typesafe.akka"         %% "akka-actor"             % "2.5.2"
)


initialCommands := "import example._"
