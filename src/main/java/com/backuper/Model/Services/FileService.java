package com.backuper.Model.Services;

import com.backuper.Model.MyFile;
import com.backuper.Model.DTOs.MyFileDTO;
import com.backuper.Model.Repositories.FileRepository;
import com.backuper.Model.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileService {

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private UserRepository ur;

    public List<MyFile> getAllFiles() {
        List<MyFile> files = new ArrayList<>();
        fileRepository.findAll()
                .forEach(files::add);
        return files;
    }

    public List<MyFileDTO> getAllFilesOfUser(Integer user_id) {
        List<MyFile> list = fileRepository.findFilesByUserId(user_id);
        List<MyFileDTO> listDTO = new ArrayList<MyFileDTO>();
        for (MyFile item : list) {
            listDTO.add(new MyFileDTO(item));
        }
        return listDTO;
    }

    public MyFile getUserFile(Integer user_id, Integer id) throws IOException, NoSuchAlgorithmException {
        return fileRepository.findOneByIdAndUserId(id, user_id);
    }

    public void addFile(MyFile file) throws IOException, NoSuchAlgorithmException {
        fileRepository.save(file);
    }

    public void deleteFile(MyFile file) throws IOException, NoSuchAlgorithmException {
        fileRepository.delete(file);
    }

    public MyFile getFile(Integer id) {
        return fileRepository.findOne(id);
    }

}
