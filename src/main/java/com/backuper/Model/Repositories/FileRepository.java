package com.backuper.Model.Repositories;

import com.backuper.Model.MyFile;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FileRepository extends CrudRepository<MyFile,Integer>{
    public List<MyFile> findFilesByUserId(Integer user_id);
    public MyFile findOneByIdAndUserId(Integer id, Integer user_id);
}
