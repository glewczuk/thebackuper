package com.thebackuper

import java.io.FileInputStream

import com.thebackuper.App.{baseAddress, fileService}
import com.thebackuper.Model.File

import scalaj.http.{Http, HttpOptions, HttpResponse, MultiPart}
import play.api.libs.json._

class Connector {

  val baseAddress = "http://localhost:8080"
  val prefixApi = "/api/user/"
  implicit val fileWrites = Json.writes[File]
  implicit val fileReads = Json.reads[File]

  def getFiles(url: String): Seq[com.thebackuper.Model.File] = {
    val response: HttpResponse[String] = Http(url).asString
    val json = Json.parse(response.body)
    json.validate[Seq[com.thebackuper.Model.File]].getOrElse(Seq())
  }

  def deleteFile(userName: String, id: Int) = {
    val url = baseAddress+prefixApi+userName+"/file/"+id+"/delete"
    Http(url).postForm.asString
  }

  def uploadFile(url: String, file: File, directory: String) = {

    val myFile = new java.io.File(directory+"/"+file.name)
    val fileIn = new FileInputStream(myFile)
    val arr = Stream.continually(fileIn.read).takeWhile(_ != -1).map(_.toByte).toArray
    val resp = Http(url)
      .postMulti(
        MultiPart(name="file", filename=file.name, mime="multipart/form-data", arr)
      ).asString
  }

  def uploadNewFile(file: java.io.File, longerName: String, userName: String,
                    mac: String, fullDirectory: String) = {
    import java.io.File
    val url = baseAddress+prefixApi+userName+"/uploadfile/"+mac+"/"+
      fileService.getRelativePath(fullDirectory, file.getAbsolutePath).replace("/", "+")+"/"+file.lastModified()

    val fileIn = new FileInputStream(new java.io.File(fullDirectory.replace("/","//")+"//"+longerName))
    val arr = Stream.continually(fileIn.read).takeWhile(_ != -1).map(_.toByte).toArray
    val resp = Http(url)
      .postMulti(
        MultiPart(name="file", filename=file.getName, mime="multipart/form-data", arr)
      ).asString
  }
}
