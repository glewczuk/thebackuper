package com.backuper.Model.Repositories;

import com.backuper.Model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Integer> {
    public List<User> findUserByLogin(String username);
}
