package com.backuper.Model.Services;

import com.backuper.Model.CurrentUser;
import com.backuper.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class CurrentUserDetailsService implements UserDetailsService {

    private final UserService userService;

    @Autowired
    public CurrentUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public CurrentUser loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userService.getUserByUsername(email);
        BCryptPasswordEncoder b = new BCryptPasswordEncoder();
        System.out.print(b.encode("lol") + " " + b.encode("lol"));
        return new CurrentUser(user);
    }
}