package com.backuper.Model.Services;

import com.backuper.Model.CurrentUser;
import com.backuper.Model.Repositories.UserRepository;
import com.backuper.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    public Integer getLoggedUserId() {
        CurrentUser cU = (CurrentUser)auth.getDetails();
        return cU.getId().intValue();
    }

    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        userRepository.findAll()
                .forEach(users::add);
        return users;
    }

    public User getUserByUsername(String username) throws UsernameNotFoundException {
        List<User> users = new ArrayList<>();
        userRepository.findUserByLogin(username)
                .forEach(users::add);
        if(users.size() != 0)
            return users.get(0);
        else
            return null;
    }

    public void addUser(User user) {
        userRepository.save(user);
    }
}
