package com.backuper.Model;

import com.backuper.Model.User;
import org.springframework.security.core.authority.AuthorityUtils;

import javax.management.relation.Role;

public class CurrentUser extends org.springframework.security.core.userdetails.User {

    private User user;

    public CurrentUser(User user) {
        super(user.getLogin(),
                user.getPassword().replace("\n", ""),
                AuthorityUtils.createAuthorityList("USER"));
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public Long getId() {
        return (long)user.getId();
    }

    public Role getRole() {
        return user.getRole();
    }

}