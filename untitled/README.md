###The backuper standalone app

#Simple script that allows to synchronize files store locally and remotely.

It is required to have sbt installed to run script.

To run type:

    sbt "run 'directory' 'login'"

  where directory is a parameter that specified your
  directory that you want synchronize with remote database
  and login is your login in 'The backuper' system.

It is not recomended to store directories in you rirectory, just flat hierarchy of files.

Bugs:
	if there are a few directories synchronizing, it is possible to delete files only from the directory in which that file was created or last modified (if deleted from other directory, file will be download from cloud service).
