package com.backuper.Controllers;

import com.backuper.Model.User;
import com.backuper.Model.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/users")
    public List<User> getAllUsers() throws IOException, NoSuchAlgorithmException {
        return userService.getAllUsers();
    }

    @RequestMapping(method=RequestMethod.POST, value="/user")
    public void addUser(@RequestBody User user) {
        userService.addUser(user);
    }
}