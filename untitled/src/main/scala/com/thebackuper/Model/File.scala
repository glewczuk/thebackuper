package com.thebackuper.Model

final case class File(id: Int, userId: Int, file: String,
                      checkSum: String, `type`: String,
                      name: String, mac: String , originalPath: String, lastModified: String)