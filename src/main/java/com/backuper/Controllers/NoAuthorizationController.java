package com.backuper.Controllers;

import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

import com.backuper.Model.CurrentUser;
import com.backuper.Model.MyFile;
import com.backuper.Model.DTOs.MyFileDTO;
import com.backuper.Model.Services.FileService;
import com.backuper.Model.Services.UserService;
import com.backuper.Model.User;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;import org.springframework.stereotype.Controller;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;
import java.util.stream.Stream;

@RestController
public class NoAuthorizationController {

    String UPLOADED_FOLDER = "//";

    @Autowired
    UserService userService;

    @Autowired
    FileService fileService;

    @PostMapping("/api/user/{login}/uploadfile/{mac}/{originalPath}/{lastModified}")
    public void handleFileUpload(@RequestParam("file") MultipartFile file,
                                 @PathVariable String login,
                                 @PathVariable String mac,
                                 @PathVariable String originalPath,
                                 @PathVariable String lastModified,
                                 RedirectAttributes redirectAttributes)
            throws IOException, NoSuchAlgorithmException {

        String originalPathFixed = originalPath.replace("+","/");
        String[] nameArr = originalPathFixed.split("/");
        String name = nameArr[nameArr.length-1];

        byte[] bytes = file.getBytes();
        Path path = Paths.get(UPLOADED_FOLDER + name);
        Files.write(path, bytes);

        User u = userService.getUserByUsername(login);
        MyFile mf = new MyFile(UPLOADED_FOLDER+name, u); //, mac, originalPathFixed, lastModified
        fileService.addFile(mf);
    }

    @RequestMapping("/api/user/{login}/file")
    public List<MyFileDTO> getAllFilesOfUserApi(@PathVariable String login) {
        Optional<User> user =
                userService.getAllUsers().stream()
                        .filter(u -> u.getLogin().equals(login))
                        .findFirst();

        return fileService.getAllFilesOfUser(user.get().getId());
    }


    @RequestMapping("api/user/{login}/file/{documentId}")
    public void download(@PathVariable String login, @PathVariable Integer documentId, HttpServletResponse response) throws IOException, NoSuchAlgorithmException {

        Optional<User> user =
                userService.getAllUsers().stream()
                        .filter(u -> u.getLogin().equals(login))
                        .findFirst();
        MyFile file = fileService.getUserFile(user.get().getId(), documentId);
        try {
            response.setHeader("Content-Disposition", "attachment;filename=\"" + file.getName()+ "\"");
            OutputStream out = response.getOutputStream();
            response.setContentType("file");
            ByteArrayInputStream baos = new ByteArrayInputStream(file.getFileContent());
            IOUtils.copy(baos , out);
            out.flush();
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(method= RequestMethod.POST, value="/api/user/{login}/file/{documentId}/delete")
    public void deleteFile(@PathVariable String login, @PathVariable Integer documentId) throws IOException, NoSuchAlgorithmException {
        Optional<User> user =
                userService.getAllUsers().stream()
                        .filter(u -> u.getLogin().equals(login))
                        .findFirst();

        MyFile file = fileService.getUserFile(user.get().getId(), documentId);
        fileService.deleteFile(file);
    }
}

